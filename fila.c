#include"fila.h"
#include<stdlib.h>

TPILHA *inicializa(){
  return NULL;
}


TPILHA *insere_pilha(int id, TPILHA *pilha){
  TPILHA *p = malloc(sizeof(TPILHA));
  p -> id = id;
  p -> prox = pilha;
  return p;
}

void libera(TPILHA *pilha){
  TPILHA *aux;
  while(pilha){
    aux = pilha;
    pilha = pilha -> prox;
    free(aux);
  }
}