#include"arquivo.h"
#define TAM_NOME 50
#define TAM_TIPO 20


//////////////MANIPULAÇÃO DE NOME PARA COLOCAR NO ARQUIVO /////////////////////
// pega o nome do arquivo a partir do numero
char *nome_a_partir_do_numero(int num){
  char *aux = (char *)malloc(sizeof(char) * 26);
  sprintf(aux, "dados/arq-%d.bin", num);
  return aux;
}
//////////// CODIGO PARA FAZER INDEX QUE NAO VAO SER REPITIR///////////////////
// cria o index

void criar_index(){
  int i = 0;
  FILE *fp = fopen("index.bin", "wb");
  if (!fp){

    exit(1);
  } 
  fwrite(&i, sizeof(int), 1, fp);
  fclose(fp);
}

// retorna o proximo index do arquivo e nao atualiza
int proximo_numero(){
  int i;
  FILE *fp = fopen("index.bin", "rb");
  if (!fp){
    exit(1);
    }
  fseek(fp, -4, SEEK_END);
  fread(&i, sizeof(int), 1, fp);
  fclose(fp);
  return (i + 1);
}

// atualiza o index do arquivo para o proximo numero
void adiciona_proximo_numero(){
  int i = proximo_numero();
  FILE *fp = fopen("index.bin", "ab");
  if (!fp){
    exit(1);
  }
  fwrite(&i, sizeof(int), 1, fp);
  fclose(fp);
}
//////////////////////OPERAÇOES FEITAS COM O ARQUIVO RAIZ ////////////////////////////

//criar o arquivo raiz esse arquivo tem duas informações o t e o numero correspondente a raiz da arvore
void salva_arquivo_raiz(int t){
  criar_index();
  FILE *fp = fopen("dados/arvore.bin", "wb");
  if (!fp){
    exit(1);
    }
  fwrite(&t, sizeof(int), 1, fp);
  fclose(fp);
  int index = criar_novo_no_no_arquivo();
  fp = fopen("dados/arvore.bin", "ab");
  fwrite(&index, sizeof(int), 1, fp);
  fclose(fp);
}
// pega o t armazenado no arquivo
int get_t_avbm(){
  int t;
  FILE *fp = fopen("dados/arvore.bin", "rb");
  if (!fp){
    exit(1);
  }
  fread(&t, sizeof(int), 1, fp);
  fclose(fp);
  return t;
}

int get_index_raiz_arvbm(){
  int t;
  FILE *fp = fopen("dados/arvore.bin", "rb");
  if (!fp){ 
    exit(1);
  }
  fread(&t, sizeof(int), 1, fp);
  fread(&t, sizeof(int), 1, fp);
  fclose(fp);
  return t;
}
void set_index_raiz_arvbm(int num){
  FILE *fp = fopen("dados/arvore.bin","rb+");
  if (!fp){
    exit(1);
  }
  fseek(fp,-4,SEEK_END);
  fwrite( &num , sizeof(int),1,fp);
  fclose(fp);
  }

//////////////////////CRIAÇÃO NA MP/////////////////////////
// cria um pizza sem nenhum informação
TPIZ *criar_pizza(){
  
  TPIZ *novo = (TPIZ *)malloc(sizeof(TPIZ));
  novo->nome = (char *)malloc(sizeof(char) * TAM_NOME);
  novo->tipo = (char *)malloc(sizeof(char) * TAM_TIPO);
  return novo;
}

// cria um no sem informaçoes
TAVBM *criar_no(){
  int t = get_t_avbm();
  TAVBM *novo = (TAVBM*)malloc(sizeof(TAVBM));
  novo -> ids = (int*) malloc(sizeof(int)* ((2 * t) - 1));
  novo -> filhos = (int *)malloc(sizeof(int) * (( 2 * t ) - 1));
  novo->pizzas  = (TPIZ **)malloc(sizeof(TPIZ*) * ((2 * t) - 1));
  int i;
  for (i = 0; i < ((2 * t) - 1);i++)
     novo -> pizzas[i] = criar_pizza();
  novo -> folha = 1;
  novo -> n_ids = 0;
  return novo;
}

/////////////////////ARMAZENAMENTO DE DADOS///////////////////////
// salvar um pizza no arquivo
void salva_pizza_no_arquivo(TPIZ *piz, FILE *fp){
  fwrite(&piz -> id, sizeof(int), 1, fp);
  fwrite(&piz -> preco, sizeof(float), 1, fp);
  fwrite(piz -> nome,sizeof(char), TAM_NOME, fp);
  fwrite(piz -> tipo,sizeof(char), TAM_TIPO, fp);

}

// salvar um no no arquivo
void salvar_no_no_arquivo(int nome, TAVBM *arv){
  int t = get_t_avbm();
  FILE *fp = fopen(nome_a_partir_do_numero(nome),"wb");
  if (!fp){
    exit(1);
    }
  fwrite(&arv->n_ids, sizeof(int), 1, fp);
  fwrite(&arv->folha, sizeof(int), 1, fp);
  fwrite(&arv->prox, sizeof(int), 1, fp);
  int i = 0;
  fwrite(arv -> ids, sizeof(int),((2*t)-1), fp);
  fwrite(arv -> filhos, sizeof(int),(2*t),fp);
  for (i =0; i < (( 2 * t )-1); i++) salva_pizza_no_arquivo(arv -> pizzas[i],fp);
  libera_no(arv);

  fclose(fp);
}

/////////////////////////LEITURA DE DADOS///////////////////////////////
// le um pizza no arquivo
TPIZ *carregar_pizza_do_arquivo(TPIZ *piz,FILE *fp){
  fread(&piz -> id, sizeof(int), 1,fp);
  fread(&piz ->preco,sizeof(float),1,fp);
  fread(piz -> nome, sizeof(char), TAM_NOME, fp);
  fread(piz -> tipo, sizeof(char), TAM_TIPO, fp);
  
  return piz;
}

// le um no do arquivo
TAVBM *carregar_no_do_arquivo(int nome){
  
  int t = get_t_avbm();
  FILE *fp = fopen(nome_a_partir_do_numero(nome), "rb");
  if (!fp) return NULL;
  TAVBM *novo = criar_no();
  
  fread(&novo -> n_ids,sizeof(int),1,fp);
  fread(&novo->folha, sizeof(int), 1, fp);
  fread(&novo -> prox, sizeof(int),1,fp);
  int i = 0;
  fread(novo -> ids,sizeof(int),((2*t)-1),fp);
  fread(novo -> filhos, sizeof(int),(2*t),fp);
  for (i = 0; i < ((2 * t) - 1); i++) carregar_pizza_do_arquivo(novo->pizzas[i], fp);
  fclose(fp);
  return novo;
}
/////////////////////CRIAR UM ARQUIVO QUE CONTENHA UM NO SEM INFORMAÇÃO////////////////////
// cria um novo no do arquivo
int criar_novo_no_no_arquivo(){
  int prox_num = proximo_numero();
  TAVBM *novo  = criar_no();
  salvar_no_no_arquivo(prox_num,novo);
  
  adiciona_proximo_numero();
  return prox_num;
}

//////////////////////LIBERAÇÃO DE DADOS NA MP/////////////////////////////
void libera_pizza(TPIZ *piz){
  
   if (((!piz->nome)||(!piz))||(!piz -> tipo)) return;

   free(piz -> nome);
   free(piz -> tipo);
   free(piz);
}

void libera_no(TAVBM *arv){
  free(arv ->ids);
  free(arv -> filhos);
  int t =get_t_avbm(),i;
  for (i = 0 ; i <((2*t)-1); i++) libera_pizza (arv -> pizzas[i]);
  
  free(arv);

}


// LE pizza

TPIZ *le_pizza(FILE *in){
  TPIZ *p = criar_pizza();
  if (0 >= fread(&p->id, sizeof(int), 1, in)){
    free(p);
    return NULL;
  }
  fread(p->nome, sizeof(char),TAM_NOME, in);
  fread(p->tipo, sizeof(char), TAM_TIPO, in);
  fread(&p->preco, sizeof(float), 1, in);
  return p;
}
