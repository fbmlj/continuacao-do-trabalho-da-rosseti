#include"arquivo.h"
#include"limits.h"
#include"fila.h"

// UMA FUNÇÃO AUXILIAR PARA PRINTAR TODAS A ARVORE
void imprime_aux(int raiz, int andar){
  if (raiz == INT_MIN) return;
  TAVBM *no = carregar_no_do_arquivo(raiz);
  
  if (no){
    int i, j;
    for (i = 0; i <= no->n_ids - 1; i++){
      if (!no -> folha) imprime_aux(no->filhos[i], andar + 1);
      for (j = 0; j <= andar; j++) printf("   ");
      printf("%d\n", no->ids[i]);
    }
    if (!no->folha) imprime_aux(no->filhos[i], andar + 1);
  }
  
}
// COPIA O VALOR DE UMA PIZZA PARA OUTRA
// PRINCIPALMENTE PARA EVITAR DA FREE DUAS VEZES NA PIZZA
void copia_pizza(TPIZ *p1, TPIZ *p2){
  p1 -> id = p2 -> id;
  p1 -> preco = p2 -> preco;
  strcpy(p1->nome,p2->nome);
  strcpy(p1->tipo, p2->tipo);
}

//FUNÇAO PRINCIPAL IMPRIME ELA PRATICAMENTE SO CHAMA AUXILIAR PEGANDO ALGUNS VALOR POR MEIO DE OUTRAS FUNÇÃO
void imprime(){
  int andar = 0;
  int raiz =  get_index_raiz_arvbm();
  imprime_aux(raiz,andar);
}
// MESMA LOGICA DO IMPRIME AUXILIAR MAS SO PARA BUSCA
int busca_aux(int num, int id){
  TAVBM *a = carregar_no_do_arquivo(num);
  if (!a) return INT_MIN;
  int i = 0;
  while ((i < a->n_ids) && (id > a->ids[i])) i++;

  
  if ((i < a->n_ids) && (a->folha) && (id == a->ids[i])) return num;
  if (a->folha) return INT_MIN;

  if (a->ids[i] == id)
     i++;
  int prox_no = a->filhos[i];
  libera_no(a);
  return busca_aux(prox_no, id);
}
// MESMA LOGICA DO IMPRIME SO PARA BUSCA
int busca(int id){
  int raiz = get_index_raiz_arvbm();
  return busca_aux(raiz, id);
}
void imprime_pizza(TPIZ *p){
  if (!p -> id) return;
  printf("%d, %s (%s), R$ %.2f\n", p->id, p->nome, p->tipo, p->preco);
}
// UTILIZA A FUNÇÃO BUSCA DE UM NO E BUSCA UM PIZZA DENTRO DESSE NO
void busca_pizza(int id){
  int arq = busca(id);
  
  TAVBM *arv = carregar_no_do_arquivo(arq);  
  if (!arv) return;
  int i = 0;
  while(arv -> ids[i] != id) i++;
  imprime_pizza(arv -> pizzas[i]);
  libera_no(arv);
}
// FUNÇÃO DE DIVISAO PARA INSERÇÃO
int divisao(int id_pai,int i, int id_original){
  
  int id_irma = criar_novo_no_no_arquivo();
  int t = get_t_avbm();
  TAVBM *arv_pai = carregar_no_do_arquivo(id_pai), *arv_original = carregar_no_do_arquivo(id_original), *arv_irma = carregar_no_do_arquivo(id_irma);
  
  arv_irma -> folha = arv_original -> folha;
  int j;
  if (!arv_original -> folha){
     
     arv_irma -> n_ids = t -1;
     for (j = 0; j < t -1; j++) arv_irma -> ids[j] = arv_original -> ids[j + t];
     for (j = 0; j < t; j++){
       arv_irma -> filhos[j] = arv_original -> filhos[j+t];
       arv_original -> filhos[j + t] = INT_MIN;
     }
  }
  else{
    arv_irma -> n_ids = t;
    for (j = 0; j < t; j++){ 
      arv_irma -> ids[j] = arv_original -> ids[j + t -1];
      arv_irma -> pizzas[j] -> id = arv_original -> pizzas[j + t -1] -> id;
      arv_irma->pizzas[j]->preco = arv_original->pizzas[j + t - 1]->preco;
      strcpy(arv_irma->pizzas[j]->tipo, arv_original->pizzas[j + t - 1]->tipo);
      strcpy(arv_irma->pizzas[j]->nome, arv_original->pizzas[j + t - 1]->nome);

    }
    arv_original -> prox = id_irma;
  }
  arv_original -> n_ids = t-1;
  for (j = arv_pai -> n_ids; j >= i; j--) arv_pai -> filhos[j+1] = arv_pai -> filhos[j];
  arv_pai -> filhos[i] = id_irma;
  for ( j = arv_pai -> n_ids; j >= i; j--) arv_pai -> ids[j] = arv_pai -> ids[j-1];
  arv_pai -> ids[i-1] = arv_original -> ids [t-1];
  arv_pai -> n_ids++;
  
  salvar_no_no_arquivo(id_pai, arv_pai);
  salvar_no_no_arquivo(id_irma, arv_irma);
  salvar_no_no_arquivo(id_original, arv_original);
  return id_pai;
     
}



int insere_nao_completo(int id_arv, TPIZ *piz){
  

  
  TAVBM *arv = carregar_no_do_arquivo(id_arv);
  
  int t = get_t_avbm();
  int i = arv -> n_ids -1;
  if (arv ->folha ){
    while((i >= 0) && (piz ->id < arv -> ids[i])){
      arv -> ids [i + 1] = arv -> ids[i];
      copia_pizza(arv -> pizzas[i+1], arv -> pizzas[i]);
      i--;
    }
    arv -> ids[i+1] = piz -> id;
    arv -> pizzas[i+1] = piz;
    arv -> n_ids ++;
    salvar_no_no_arquivo(id_arv, arv);
    return id_arv;
  }
  while((i>=0) && (piz ->id < arv -> ids[i])){ 
    i--;
  }
  i++;
  TAVBM *filho = carregar_no_do_arquivo(arv -> filhos[i]);
  int id_filho = arv -> filhos[i];
  if( filho -> n_ids == ((2 * t)-1) ){

    salvar_no_no_arquivo(id_arv, arv);
    id_arv = divisao(id_arv,(i+1),id_filho);
    
    // imprime();
    arv = carregar_no_do_arquivo(id_arv);
    
    if(piz ->id > arv -> ids[i]) i++;
    
  }
  libera_no(filho);
  
  arv -> filhos[i] = insere_nao_completo(arv -> filhos[i], piz);
  return id_arv;
}


int insere_aux(int id_arv, TPIZ *piz ){
  int t = get_t_avbm();
  TAVBM *arv = carregar_no_do_arquivo(id_arv);
  
  if (!arv) return id_arv;
  if(arv -> n_ids == (( 2 * t ) - 1 )){
     int pai = criar_novo_no_no_arquivo();
     
     TAVBM *arv_pai = carregar_no_do_arquivo(pai);
     arv_pai -> folha = 0;
     arv_pai -> n_ids = 0;
     arv_pai -> filhos[0] = id_arv;
     salvar_no_no_arquivo(pai,arv_pai);
     
     pai = divisao(pai, 1 , id_arv);
     pai = insere_nao_completo(pai, piz);
     return pai;

  }
  salvar_no_no_arquivo(id_arv, arv);
  
  id_arv = insere_nao_completo(id_arv, piz);
  return id_arv;
}

void insere_pizz(TPIZ *piz){
  if (busca(piz->id) != INT_MIN) return;
  int raiz = get_index_raiz_arvbm();
  set_index_raiz_arvbm(insere_aux(raiz, piz));
}

void insere(){
  printf("Escreva o id da pizza quer adicionar:   ");
  int id;
  scanf("%d",&id);
  if (busca(id) != INT_MIN){
    printf("<<< ID JA UTILIZADO!!!! >>>\n");
    return;
  }
  TPIZ *piz =  criar_pizza();
  piz -> id = id;
  printf("Escreva o nome da pizza:  ");
  scanf("%s", piz -> nome);
  printf("Escreva a classificação da pizza:   ");
  scanf("%s", piz -> tipo);
  printf("Escreva o preço:   ");
  scanf("%f", &piz -> preco);
  int raiz = get_index_raiz_arvbm();
  set_index_raiz_arvbm(insere_aux(raiz, piz));
}

// PECORRE AR ARVORE PARA ESQUERDA PARA PEGAR O NO MAIS A ESQUERDA
int get_arquivo_mais_ad_esquerda(){
  int aux = get_index_raiz_arvbm();
  TAVBM *arv = carregar_no_do_arquivo(aux);
  while (!arv -> folha)  {
    aux = arv ->filhos[0];
    free(arv);
    arv = carregar_no_do_arquivo(aux);
  }
  free(arv);
  return aux;
}
// PECORRE AS FOLHAS COMO UMA LISTA ENCADEADA
TAVBM *vai_para_o_proximo(TAVBM *arvbm){
  int prox = arvbm -> prox;
  libera_no(arvbm);
  return carregar_no_do_arquivo(prox);
}

void imprime_todas_as_pizza(){
  int atual = get_arquivo_mais_ad_esquerda();
  TAVBM *arv = carregar_no_do_arquivo(atual);
  while(arv){
    int j;

    for (j = 0; j < arv->n_ids; j++) imprime_pizza(arv -> pizzas[j]);
    arv = vai_para_o_proximo(arv);
  }
}

void imprime_todas_as_pizza_dessa_categoria(char *tipo){
  int atual = get_arquivo_mais_ad_esquerda();
  TAVBM *arv = carregar_no_do_arquivo(atual);
  while (arv)  {
    int j;
    for (j = 0; j < arv -> n_ids; j++){
       if (!strcmp(arv -> pizzas[j]->tipo,tipo)) imprime_pizza(arv -> pizzas[j]);
    }
    arv = vai_para_o_proximo(arv);

  }
  
}
// ESSA FUNÇÃO PEGA UM NO E DESLOCA TODOS OS SEUS ELEMENTOS PARA DIREITA
TAVBM *desloca_para_direita(TAVBM *arvore, int t){
  int j;
  for (j = arvore->n_ids; j != 0; j--){
    if (j >= 2 * t)
      continue;
    arvore->ids[j] = arvore->ids[j - 1];
    copia_pizza(arvore -> pizzas[j], arvore->pizzas[j - 1]);
  }
  for (j = arvore-> n_ids + 1; j != 0; j--){
    if (j >= 2 * t)
      continue;
    arvore->filhos[j] = arvore->filhos[j - 1];
  }
  if (arvore->n_ids < 2 * t - 1) arvore->n_ids++;
  return arvore;
}
// ESSA FUNÇÃO PEGA UM NO E DESLOCA TODOS OS SEUS ELEMENTOS PARA ESQUERDA
TAVBM *desloca_para_esquerda(TAVBM *arvore, int t){
  int j;
  for (j = 0; j < arvore->n_ids - 1; j++){
    arvore->ids[j] = arvore->ids[j + 1];
    copia_pizza(arvore -> pizzas[j], arvore -> pizzas[j+1]);
  }
  for (j = 0; j < arvore->n_ids; j++)
    arvore->filhos[j] = arvore->filhos[j + 1];
  if (arvore->n_ids)
    arvore->n_ids--;
  return arvore;
}
// CASO 3B
int caso_3a(int id_arv, int pos_filho, int pos_irma){
  int t = get_t_avbm();
  TAVBM *arv = carregar_no_do_arquivo(id_arv);
  int id_filho = arv->filhos[pos_filho];
  TAVBM *filho = carregar_no_do_arquivo(id_filho);
  int id_irma = arv->filhos[pos_irma];
  TAVBM *irma = carregar_no_do_arquivo(id_irma);
  if (!((arv) && (filho) && (irma))){
    exit(1);
  }
  if (filho -> folha){
    if (pos_irma < pos_filho){
      
      int pos_pai = pos_irma;
      arv -> ids[pos_pai] = irma -> ids[irma -> n_ids -1];
      filho = desloca_para_direita(filho,t);
      filho->ids[0] = irma->ids[irma->n_ids - 1];
      copia_pizza(filho->pizzas[0], irma->pizzas[irma->n_ids - 1]);
      irma -> n_ids --;
      salvar_no_no_arquivo(id_irma, irma);
      salvar_no_no_arquivo(id_filho, filho);
      salvar_no_no_arquivo(id_arv, arv);
      return id_arv;
    }
    else{

      
      int pos_pai = pos_filho;
      arv -> ids[pos_pai] = irma -> ids[1];
      
      filho->ids[filho->n_ids] = irma->ids[0];
      copia_pizza(filho->pizzas[filho->n_ids], irma->pizzas[0]);
      filho->n_ids ++;
      irma = desloca_para_esquerda(irma, t);
      salvar_no_no_arquivo(id_irma, irma);
      salvar_no_no_arquivo(id_filho, filho);
      salvar_no_no_arquivo(id_arv, arv);
      return id_arv;
    }
  }

  if (pos_irma < pos_filho){
      int pos_pai = pos_irma;
      filho = desloca_para_direita(filho,t);
      filho -> ids[0] = arv -> ids[pos_pai];
      filho -> filhos[0] = irma -> filhos[irma -> n_ids];
      arv -> ids[pos_pai] = irma -> ids[irma -> n_ids -1];
      irma -> n_ids --;
      salvar_no_no_arquivo(id_irma, irma);
      salvar_no_no_arquivo(id_filho, filho);
      salvar_no_no_arquivo(id_arv, arv);
      return id_arv;
  }
  else{
    int pos_pai = pos_filho;
    filho -> ids[filho->n_ids] = arv -> ids[pos_pai];
    filho -> filhos[filho->n_ids + 1] = irma -> filhos[0];
    arv -> ids[pos_pai] = irma -> ids[0];
    irma = desloca_para_esquerda(irma,t);
    filho -> n_ids++;
    salvar_no_no_arquivo(id_irma, irma);
    salvar_no_no_arquivo(id_filho, filho);
    salvar_no_no_arquivo(id_arv, arv);
    return id_arv;
  }
}




int caso_3b(int id_arv,int pos_filho, int pos_irma){
  int t = get_t_avbm();
  TAVBM *arv = carregar_no_do_arquivo(id_arv);
  int id_filho = arv -> filhos[pos_filho];
  TAVBM *filho = carregar_no_do_arquivo(id_filho);
  int id_irma = arv->filhos[pos_irma];
  TAVBM *irma = carregar_no_do_arquivo(id_irma);
  if (!((arv)&&(filho)&&(irma))){
    exit(1);
  }
  if (filho -> folha){
    if (pos_irma < pos_filho){
      int j;
      for (j = 0; j < filho -> n_ids; j++){
        irma -> ids[irma -> n_ids + j] = filho -> ids[j];
        copia_pizza(irma->pizzas[irma->n_ids + j], filho->pizzas[j]);
      }
      irma -> n_ids += filho -> n_ids;
      irma -> prox = filho -> prox;
      int aux1 = arv -> filhos[arv -> n_ids];
      int pos_pai = pos_irma;
      for (j = pos_pai; j < arv->n_ids - 1; j++)arv->ids[j] = arv->ids[j + 1];
      for (j = pos_pai; j < arv->n_ids; j++){

        arv->filhos[j] = arv->filhos[j + 1];
      }
      arv -> filhos[pos_irma] = id_irma;
      arv -> n_ids --;
      // arv -> filhos[arv -> n_ids] = aux1;
      int aux = arv -> n_ids;
      salvar_no_no_arquivo(id_irma,irma);
      salvar_no_no_arquivo(id_filho,filho);
      salvar_no_no_arquivo(id_arv, arv);
      if (!aux)return id_irma;
      return id_arv;
      }
      int j;
      for (j = 0; j < irma->n_ids; j++){
        filho->ids[filho->n_ids + j] = irma->ids[j];
        copia_pizza(filho->pizzas[filho->n_ids + j], irma->pizzas[j]);
      }
      filho -> n_ids += irma -> n_ids;
      filho -> prox = irma -> prox;
      int pos_pai = pos_filho;
      for (j = pos_pai; j < arv->n_ids - 1; j++) arv->ids[j] = arv->ids[j + 1];
      for (j = pos_pai; j < arv->n_ids; j++) arv->filhos[j] = arv->filhos[j + 1];
      arv->filhos[pos_pai] = id_filho;
      arv->n_ids--;
      int aux = arv->n_ids;
      salvar_no_no_arquivo(id_irma, irma);
      salvar_no_no_arquivo(id_filho, filho);
      salvar_no_no_arquivo(id_arv, arv);
      if (!aux)return id_filho;
      return id_arv;
    }

    if(pos_irma < pos_filho){
      int j;
      int pos_pai = pos_irma;
      filho = desloca_para_direita(filho,t);
      filho -> ids[0] = arv -> ids[pos_pai];
      filho -> filhos[0] = irma -> filhos[irma ->n_ids];
      for (j = 0; j < irma->n_ids; j++){
        filho = desloca_para_direita(filho, t);
        filho->ids[0] = irma->ids[irma->n_ids - j - 1];
      }

      for (j = 0; j < irma->n_ids; j++)filho->filhos[j] = irma->filhos[j];
      
      for (j = pos_pai; j < arv->n_ids - 1; j++) arv->ids[j] = arv->ids[j + 1];
      for (j = pos_pai; j < arv->n_ids; j++) arv->filhos[j] = arv->filhos[j + 1];
      arv->n_ids--;
      int aux = arv->n_ids;
      salvar_no_no_arquivo(id_irma, irma);
      salvar_no_no_arquivo(id_filho, filho);
      salvar_no_no_arquivo(id_arv, arv);
      if (!aux) return id_filho;
      return id_arv;
    }
    int pos_pai = pos_filho,j;
    filho->ids[filho->n_ids] = arv->ids[pos_pai];
    filho->filhos[filho->n_ids + 1] = irma->filhos[0];
    filho->n_ids++;
    
    for (j = filho->n_ids; j < (filho->n_ids + irma->n_ids); j++) filho->ids[j] = irma->ids[j - filho->n_ids];
    for (j = 0; j < irma->n_ids; j++){
      
      filho->filhos[j + filho->n_ids + 1] = irma->filhos[j + 1];

    }
    filho->n_ids += irma->n_ids;
    
    for (j = pos_pai; j < arv->n_ids - 1; j++) arv->ids[j] = arv->ids[j + 1];
    for (j = pos_pai; j < arv->n_ids; j++)  arv->filhos[j] = arv->filhos[j + 1];
    
    arv->n_ids--;
    int aux = arv->n_ids;
    arv -> filhos[pos_pai] = id_filho;
    salvar_no_no_arquivo(id_irma, irma);
    salvar_no_no_arquivo(id_filho, filho);
    salvar_no_no_arquivo(id_arv, arv);
    
    if (!aux)return id_filho;
    return id_arv;
  }

int retira_aux(int id_arv,int info, int t){
  
  TAVBM *arv = carregar_no_do_arquivo(id_arv);
  if (!arv) return INT_MIN;
  int i = 0;
  if (arv -> folha){
 
    while ((i < arv -> n_ids) && (info > arv -> ids[i]))i++;
    // CASO 1 NA REMOÇÃO
    if (arv -> ids[i] == info ){
      arv ->n_ids --;
      int j;
      for (j = i;j < arv ->n_ids;j++){
        arv -> ids[j] = arv ->ids[j+1];
        copia_pizza(arv -> pizzas[j], arv -> pizzas[j+1]);
      }

      salvar_no_no_arquivo(id_arv, arv);
      return id_arv;
      
    }
    //ELEMENTO NAO ESTA NA ARVORE
    
    printf("Não existe pizza com esse id na arvore\n");
    return id_arv;
  }
  while ((i < arv->n_ids) && (info >= arv->ids[i]))i++;
  TAVBM *filho = carregar_no_do_arquivo(arv -> filhos[i]);
  
  //TRATAMENTO PARA RETIRAR
  if (filho -> n_ids == (t-1)){
    
    //ESSE CODIGO DA PRIORIDADE SEMPRE UTILIZAR O NO IRMA ATRAS DELE PARA TRATAR A EXEÇÃO
    
    if (i){
      TAVBM *irma = carregar_no_do_arquivo(arv -> filhos[i-1]);
      if (irma -> n_ids != (t-1)){
        libera_no(irma);
        libera_no(filho);
        libera_no(arv);
        id_arv = caso_3a(id_arv, i, i - 1);
         
        return retira_aux(id_arv, info, t);
      }
      libera_no(irma);
    }
    if (i != arv -> n_ids){
      TAVBM *irma = carregar_no_do_arquivo(arv -> filhos[i+1]);
      if (irma->n_ids != (t - 1)){
        libera_no(irma);
        libera_no(filho);
        libera_no(arv);
        id_arv = caso_3a(id_arv, i, i + 1);

        return retira_aux(id_arv, info, t);
      }
      libera_no(irma);
    }
    if (i){
      TAVBM *irma = carregar_no_do_arquivo(arv -> filhos[i-1]);
      if (irma -> n_ids == (t-1)){
        libera_no(irma);
        libera_no(filho);
        libera_no(arv);
        id_arv = caso_3b(id_arv, i, i - 1);
         
        return retira_aux(id_arv, info, t);
      }
      libera_no(irma);
    }
    if (i != arv -> n_ids){
      TAVBM *irma = carregar_no_do_arquivo(arv -> filhos[i+1]);
      if (irma->n_ids == (t - 1)){
        libera_no(irma);
        libera_no(filho);
        libera_no(arv);
        id_arv = caso_3b(id_arv, i, i + 1);

        return retira_aux(id_arv, info, t);
      }
      libera_no(irma);
    }


    //   if (i){

    //     TAVBM *irma = carregar_no_do_arquivo(arv -> filhos[i-1]);
    //     libera_no(arv);

    //     //Caso 3B
    //     if (irma -> n_ids == (t-1)){

    //       libera_no(irma);
    //       libera_no(filho);

    //       id_arv = caso_3b(id_arv,i, i -1);
    //       return retira_aux(id_arv,info,t);
    //     }
    //     //Caso 3A

    //     libera_no(irma);
    //     libera_no(filho);
    //     id_arv = caso_3a(id_arv, i, i - 1);

    //     return retira_aux(id_arv, info, t);
    //   }

    //   TAVBM *irma = carregar_no_do_arquivo(arv->filhos[i + 1]);
    //   libera_no(arv);
    //   //Caso 3B
    //   if (irma->n_ids == (t - 1)){

    //     libera_no(irma);
    //     libera_no(filho);
    //     id_arv = caso_3b(id_arv, i, i + 1);
    //     return retira_aux(id_arv, info, t);
    //   }
    //   //Caso 3A
    //   libera_no(irma);
    //   libera_no(filho);
    //   id_arv = caso_3a(id_arv, i, i + 1);

    //   return retira_aux(id_arv, info, t);

    }

    //SEM TRATAMENTO PARA RETIRAR
    libera_no(filho);

    arv->filhos[i] = retira_aux(arv->filhos[i], info, t);
    salvar_no_no_arquivo(id_arv, arv);
    return id_arv;
}


void retira(int id){
  
  int t = get_t_avbm();
  int raiz = get_index_raiz_arvbm();
  set_index_raiz_arvbm(retira_aux(raiz,id,t));

}

void remover_todas_as_pizza_dessa_categoria(char *tipo){
  int atual = get_arquivo_mais_ad_esquerda();
  TAVBM *arv = carregar_no_do_arquivo(atual);
  TPILHA *lista = inicializa();
  while (arv){
    int j;
    for (j = 0; j < arv->n_ids; j++){
      if (!strcmp(arv->pizzas[j]->tipo, tipo))
        lista = insere_pilha(arv->pizzas[j]->id, lista);
    }
    
    arv = vai_para_o_proximo(arv);
    
  }
  while(lista){
    retira(lista->id);

    lista = lista ->prox;
  }
  libera(lista);
}

void atualiza_pizza(){
  printf("Escreva o id da pizza quer adicionar:   ");
  int id;
  scanf("%d", &id);
  TPIZ *piz = criar_pizza();
  piz->id = id;
  printf("Escreva o nome da pizza:  ");
  scanf("%s", piz->nome);
  printf("Escreva a classificação da pizza:   ");
  scanf("%s", piz->tipo);
  printf("Escreva o preço:   ");
  scanf("%f", &piz->preco);

  int arq = busca(id);
  TAVBM *arv = carregar_no_do_arquivo(arq);
  if (!arv)
    return;
  int i = 0;
  while (arv->ids[i] != id)
    i++;
  copia_pizza(arv->pizzas[i], piz);
  libera_pizza(piz);
  imprime_pizza(arv->pizzas[i]);
  salvar_no_no_arquivo(arq, arv);
}