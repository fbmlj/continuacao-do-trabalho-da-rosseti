typedef struct pilha
{
  int id;
  struct pilha *prox;
}TPILHA;

TPILHA *inicializa();
TPILHA *insere_pilha(int id, TPILHA *pilha);
void libera(TPILHA *pilha);