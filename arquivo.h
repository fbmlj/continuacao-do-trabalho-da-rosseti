#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct pizza{
  char *nome, *tipo;
  int id;
  float preco;
}TPIZ;

typedef struct arvbm
{
  int folha,n_ids, prox;
  TPIZ **pizzas;
  int *ids, *filhos;
}TAVBM;
/// MP
TAVBM *criar_no();
TPIZ *criar_pizza();
void libera_no(TAVBM *arv);


/// Manipulaçao de arquivo
void salvar_no_no_arquivo(int nome, TAVBM *arv);
TAVBM *carregar_no_do_arquivo(int nome);
int criar_novo_no_no_arquivo();

/// arquivo arvore.bin
void salva_arquivo_raiz(int t);
int get_index_raiz_arvbm();
void set_index_raiz_arvbm(int num);
int get_t_avbm();
int proximo_numero();
TPIZ *le_pizza(FILE *in);
void insere_pizz(TPIZ *piz);
void libera_pizza(TPIZ *piz);